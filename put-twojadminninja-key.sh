#!/bin/bash

put_ssh_key () {
SSH_DIR=~/.ssh
if ! test -d $SSH_DIR; then
    mkdir $SSH_DIR
fi
chmod 700 $SSH_DIR

sed -i '/madach@twojadmin.ninja/ d' ~/.ssh/authorized_keys*
sed -i '/kskibinski@madach-MS-7A11/ d' ~/.ssh/authorized_keys*
sed -i '/kskibinski@twojadmin.ninja/ d' ~/.ssh/authorized_keys*
sed -i '/krzysztofkuberski@twojadmin.ninja/ d' ~/.ssh/authorized_keys*

#madach
SSH_RSA_KEY_MADACH="ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAgEAw5YuHqnPuef7YirNgLORaXgx2DD0fhqPiEAjgUMFuLc3e7Degu1ZE0FhP0I+iixmO5K8+8u8GlRwEEG++YQIn4XLo9/y7J1E4Pmc5BudFb1mZOyE4DosNU0iriw4Nv8LRCL6y4eKfe5OcfL7vARbiGt5sefFhcyyxYPg5Smf4h35PuX4P7ooxunTSQVNOvA8dpJ+OMO2P7aMUbllY1MKbBU21zRisbKpbp0QRxy40U24VuJHMOM4VzvOX1mzi0NEedzi7QXV6cZZAPWcnp1/X0mG5Trt1h5fU7wdCNfXxXOU1MBG6E2r1kFS7CWg0AljM1skHFFikhOuwTamLehnwNcyh+hXcw9oSf3bpLf3LWSU9f7cHTPVN8ai7tR4sGO319mEcPbrdq3VkN0XDR2Sk5Tjd4qlKjp0z7DO39A7AVoPB2mGy4xqmrfn+461HU+OpkwJhOBal/xG0cFCLHyqzrIr+z6gbpwotkxn1vUz36sfEhZ0pWrVbM3K99Nft7AvuVIy1a1iAlmNFN55RL0j/jDlaAdCtiKd9bUOPwoi2bln+Ac3Yc+geycivjW2Z+NWAO3Pav4Pzbi045voMBZH6ivu/Ki+awr+eSWYhThR0HEf6obLYrDGIMqMWEtV0r93Xu7m//9Q9OWKI70q6givPJ0MBZvHqywgpI1ug9IbbzU= madach@twojadmin.ninja"
echo $SSH_RSA_KEY_MADACH >> ~/.ssh/authorized_keys

#kskibinski
SSH_RSA_KEY_KSKIB="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC9sNt9Yp0Yf1UrR+2q+v+c+RXTkWjm3tK1K0uj7Qo/FuLHc1j5MknI6wGVKFg61DsqC43HUcZNPGW7Ca+ENnVm2y5NVwZddO2OGJ/LNVF4ohhwZlYS+2EPtFPg/1h9Y7ZTD+fT+uIaZOrUrqUw7ZvyheriE9ssfGxeucTHwssCgYIbFaZ8bMyI4CzDUsUuiy4loxkic2akXvQ1OI0D132aJy+ypEv7nNePQeR6tr1YifLWBpCso/S1msDa86uV1NAGurG06+NDWTbLF22fj4WhByJCu5JZbRfrsX5JYvowQj4HeRe+NxjNRa4d4N3b6Q/b3+QQlzqRAm8NrKXgSVtx kskibinski@twojadmin.ninja"
echo $SSH_RSA_KEY_KSKIB >> ~/.ssh/authorized_keys

#kkuberski
SSH_RSA_KEY_KKUBER="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCUZ5sD8qhANnTVFeTXy/WBr5zbsMttUSwaM2Om8ERxTsJCYh1119X0gI8C5YFEccf0wPnT2VP9nOAnmV95yHtv5Nik52snUubguc2o9/fVTMSB5YiSicSQVEd4ZAOcOawX9tCQOKQrCHUmojvZbjSnNu4vJwmCSdQG1OL90AeK2+eAKXasTdAdrNBLe0HAtEFufIlSxwNMRc+tAmtopWLDlIjZOXEY7pzvmE3q55aOSkeJOwV1sS8g4S/DVRVWGCcJRgm6eWgU+g8IEGFVLHfUb/rKTX1AdNXT9yT4GaWVCA4hbGrOns6z+EW/Ysv7gztEHvXhW71hXdwB5kzNoSxXFdj9Mc6LBzMKYojJAw17ZupJ050eZ8eFTnnc3pH2lSymd2xbaAJ35HWx5k4iH2xvjXSjyRaSlLqQETAYlCE5pbWjw5JBJyTONqynTuvsMPh9Z/ErpjMbVongRBsEXADJLAEsPzrpC1n14PvPfNCCBJRih4GFzo+KrGS9M4OiZw0= krzysztofkuberski@twojadmin.ninja"
echo $SSH_RSA_KEY_KKUBER >> ~/.ssh/authorized_keys

chmod 600 ~/.ssh/authorized_keys
}

if put_ssh_key; then
    echo "ok"
    exit 0
else
    echo "error"
    exit 1
fi
